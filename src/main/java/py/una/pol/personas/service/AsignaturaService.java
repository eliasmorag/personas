package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation

// signigica que son unos beans de sesion que van a vivir en el servidor
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre() );
    }
    
    public void matricular(int cedula, int codigo) throws Exception {
    	log.info("Matriculando alumno");
    	try {
    		dao.matricular(cedula, codigo);
    	}catch(Exception e) {
    		log.severe("ERROR al matricular");
    	}
    	log.info("Matriculacion exitosa");
    }
    
    public void desmatricular(int cedula, int codigo) throws Exception {
    	log.info("Desmatriculando alumno");
    	try {
    		dao.desmatricular(cedula, codigo);
    	}catch(Exception e) {
    		log.severe("ERROR al desmatricular");
    	}
    	log.info("Desmatriculacion exitosa");
    }
    
    public void actualizar(Asignatura a) throws Exception {
    	log.info("Modificando Asignatura: " + a.getNombre());
    	try {
    		dao.actualizar(a);
    	}catch(Exception e) {
        	log.severe("ERROR al modificar asignatura: " + e.getLocalizedMessage() );
        	throw e;
    	}
        log.info("Asignatura modificada con éxito: " + a.getNombre() );

    }
    
    public List<String> asignaturasPorAlumno (int cedula) {
    	return dao.asignaturasPorAlumno(cedula);
    }
    
    public List<String> alumnosPorAsignatura (int codigo) {
    	return dao.alumnosPorAsignatura(codigo);
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorCodigo(long codigo) {
    	return dao.seleccionarPorCodigo(codigo);
    }
    
    public long borrar(long codigo) throws Exception {
    	return dao.borrar(codigo);
    }
}
