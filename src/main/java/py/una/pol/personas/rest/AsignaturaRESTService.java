package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.service.AsignaturaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de personas
 */
@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService {

    @Inject
    private Logger log;

    @Inject
    AsignaturaService asignaturaService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar() {
        return asignaturaService.seleccionar();
    }

    @GET
    @Path("/{codigo:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerPorId(@PathParam("codigo") long codigo) {
        Asignatura a = asignaturaService.seleccionarPorCodigo(codigo);
        if (a == null) {
        	log.info("obtenerPorId " + codigo + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + codigo + " encontrada: " + a.getNombre());
        return a;
    }

    @GET
    @Path("/codigo")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerPorIdQuery(@QueryParam("codigo") long codigo) {
        Asignatura a = asignaturaService.seleccionarPorCodigo(codigo);
        if (a == null) {
        	log.info("obtenerPorId " + codigo + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + codigo + " encontrada: " + a.getNombre());
        return a;
    }
    
    @GET
    @Path("/cedula")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> asignaturasPorAlumno(@QueryParam("cedula") int cedula) {
        List<String> listado = asignaturaService.asignaturasPorAlumno(cedula);
        if (listado == null) {
        	log.info("asignaturasPorAlumno " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("asignaturasPorAlumno " + cedula + " encontradas: " + listado.size());
        return listado;
    }

    @GET
    @Path("/inscriptos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> alumnosPorAsignatura(@QueryParam("codigo") int codigo) {
        List<String> listado = asignaturaService.alumnosPorAsignatura(codigo);
        if (listado == null) {
        	log.info("alumnosPorAsignatura " + codigo + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("alumnosPorAsignatura " + codigo + " encontrados: " + listado.size());
        return listado;
    }
    
    
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asignatura a) {

        Response.ResponseBuilder builder = null;
        
        try {
        	        	
	            asignaturaService.crear(a);
	            // Create an "ok" response
	            
	            //builder = Response.ok();
	            builder = Response.status(201).entity("Asignatura creada exitosamente");
	            
	 		 
        	}catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Asignatura a) {

        Response.ResponseBuilder builder = null;
        
        try {
        	        	
	            asignaturaService.actualizar(a);
	            // Create an "ok" response
	            
	            //builder = Response.ok();
	            builder = Response.status(201).entity("Asignatura actualizada exitosamente");
	            
	 		 
        	}catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    
    @POST
	@Path("/{cedula}/{codigo}")
    public Response matricular( @PathParam("cedula") int cedula,
								@PathParam("codigo") int codigo) {
    	
        Response.ResponseBuilder builder = null;
        try {
        	asignaturaService.matricular(cedula, codigo);
        	builder = Response.status(201).entity("Matriculacion exitosa");
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();

    }
    
    @DELETE
	@Path("/{cedula}/{codigo}")
    public Response desmatricular(	@PathParam("cedula") int cedula,
									@PathParam("codigo") int codigo) {
    	Response.ResponseBuilder builder = null;
 	   	try {

 			   asignaturaService.desmatricular(cedula, codigo);
 			   builder = Response.status(202).entity("Desmatriculacion exitosa.");
 
 	   	} catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();

    }
    

   @DELETE
   @Path("/{codigo}")
   public Response borrar(@PathParam("codigo") long codigo)
   {      
	   Response.ResponseBuilder builder = null;
	   try {
		   
		   if(asignaturaService.seleccionarPorCodigo(codigo) == null) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
		   }else {
			   asignaturaService.borrar(codigo);
			   builder = Response.status(202).entity("Asignatura borrada exitosamente.");			   
		   }
		   

		   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }
   
}
