package py.una.pol.personas.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura  implements Serializable {

	Long codigo;
	String nombre;
		
	public Asignatura(){

	}

	public Asignatura(Long pcodigo, String pnombre){
		this.codigo = pcodigo;
		this.nombre = pnombre;
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
